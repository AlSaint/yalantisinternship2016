package com.intership.yalantis.com.taskone.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.intership.yalantis.com.taskone.activities.TaskOneActivity;
import com.intership.yalantis.com.taskone.models.Icon;
import com.intership.yalantis.com.taskone.models.AllocationData;
import com.intership.yalantis.com.taskone.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainListViewAdapter extends BaseAdapter {

    private LayoutInflater mInflater = null;
    private List<AllocationData> mData;
    private static final Icon[] sIcons = Icon.values();

    public MainListViewAdapter(Context context, List<AllocationData> data) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mData = data;
    }

    @Override
    public int getCount() {
        return mData != null ? mData.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();

        } else {
            convertView = mInflater.inflate(R.layout.main_item_linear_layout, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.mImageViewIcon.setImageResource(sIcons[position].getDrawableId());
        holder.mLikesCount.setText(mData.get(position).getLikesCount());
        holder.mHeadline.setText(mData.get(position).getHeadLine());
        holder.mAddress.setText(mData.get(position).getAddress());
        holder.mDate.setText(mData.get(position).getDate());
        holder.mDaysLeft.setText(mData.get(position).getDaysLeft());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), TaskOneActivity.class);
                v.getContext().startActivity(intent);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.main_list_item_event_icon_image_view)
        ImageView mImageViewIcon;
        @Bind(R.id.main_list_item_likes_count_text_view)
        TextView mLikesCount;
        @Bind(R.id.main_list_item_headline)
        TextView mHeadline;
        @Bind(R.id.main_list_item_address_text_view)
        TextView mAddress;
        @Bind(R.id.main_list_item_date_text_view)
        TextView mDate;
        @Bind(R.id.main_list_item_days_left_text_view)
        TextView mDaysLeft;

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
    }
}