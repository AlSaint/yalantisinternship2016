package com.intership.yalantis.com.taskone.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intership.yalantis.com.taskone.activities.TaskOneActivity;
import com.intership.yalantis.com.taskone.models.AllocationData;
import com.intership.yalantis.com.taskone.models.Icon;
import com.intership.yalantis.com.taskone.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder> {
    private List<AllocationData> mData;
    private static final Icon[] sIcons = Icon.values();

    /**
     * Whereas an ArrayAdapter requires a context to be passed into it's constructor, a RecyclerView.Adapter does not.
     * Instead, the correct context can inferred from the parent view when inflation is necessary.
     */
    public MainRecyclerViewAdapter(List<AllocationData> data) {
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_item_linear_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.mImageViewIcon.setImageResource(sIcons[position].getDrawableId());
        viewHolder.mLikesCount.setText(mData.get(position).getLikesCount());
        viewHolder.mHeadline.setText(mData.get(position).getHeadLine());
        viewHolder.mAddress.setText(mData.get(position).getAddress());
        viewHolder.mDate.setText(mData.get(position).getDate());
        viewHolder.mDaysLeft.setText(mData.get(position).getDaysLeft());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.main_list_item_event_icon_image_view)
        ImageView mImageViewIcon;
        @Bind(R.id.main_list_item_likes_count_text_view)
        TextView mLikesCount;
        @Bind(R.id.main_list_item_headline)
        TextView mHeadline;
        @Bind(R.id.main_list_item_address_text_view)
        TextView mAddress;
        @Bind(R.id.main_list_item_date_text_view)
        TextView mDate;
        @Bind(R.id.main_list_item_days_left_text_view)
        TextView mDaysLeft;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), TaskOneActivity.class);
                    v.getContext().startActivity(intent);
                }
            });
        }
    }
}